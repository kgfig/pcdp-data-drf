from django.db import models

from assessment.models import Answer, Project

class PCDPUser(models.Model):
    firstname = models.CharField(max_length=64)
    lastname = models.CharField(max_length=64)
    username = models.CharField(max_length=32)
    original_id = models.IntegerField(default=0)
    email = models.EmailField(max_length=254)
    project = models.ForeignKey(
        Project,
        default=None,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='users',
        related_query_name='user'
    )
    answers = models.ManyToManyField(Answer)
    

    class Meta:
        unique_together = (('username', 'email', 'original_id', 'project',),)
