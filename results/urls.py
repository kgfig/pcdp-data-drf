from django.conf.urls import include, url
from results.views import ResultDetailView

urlpatterns = [
    url(r'^(?P<assessment_id>\d+)/$', ResultDetailView.as_view(), name='detail')
]
