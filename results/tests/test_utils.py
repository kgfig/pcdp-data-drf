from django.test import TestCase
import os

from results.utils import Importer

class ImporterTest(TestCase):

    def test_init(self):
        path = os.path.abspath(os.path.join(os.path.join('..', 'data'), 'betatest_phase1.zip'))
        importer = Importer(path)
        num_files = 17
        self.assertEqual(num_files, len(importer.files))

        
        
