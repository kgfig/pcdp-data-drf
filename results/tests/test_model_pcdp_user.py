from django.test import TestCase

from results.models import PCDPUser
from assessment.models import Project, Answer

class PCDPUserTest(TestCase):

    def test_can_save_and_get_user(self):
        user = PCDPUser(firstname='Juanita', lastname='dela Cruz', username='juandc', original_id=0, email='jdc@email.com')
        user.save()

        saved_user = PCDPUser.objects.first()
        self.assertEqual(saved_user, user)
        
    
    def test_user_is_associated_with_a_project(self):
        project = Project(name='Important Project')
        project.save()
        
        user = PCDPUser(firstname='Juanita', lastname='dela Cruz', username='juandc', original_id=0, email='jdc@email.com', project=project)
        user.save()

        saved_user = PCDPUser.objects.first()
        self.assertEqual(saved_user.project, project)

    def test_user_can_have_answers(self):
        project = Project(name='Important Project')
        project.save()
        
        answer = Answer(text='Choice A', label='A', original_id=0, correct=False)
        second_answer = Answer(text='Choice B', label='B', original_id=0, correct=True)
        answer.save()
        second_answer.save()

        user = PCDPUser(firstname='Juanita', lastname='dela Cruz', username='juandc', original_id=0, email='jdc@email.com', project=project)
        user.save()
        user.answers.add(answer)
        user.answers.add(second_answer)
        user.save()

        saved_user = PCDPUser.objects.first()
        saved_answers = Answer.objects.all().order_by('label')
        # assertQuerysetEqual takes a query set, a list of values and a callable function
        # applied to the queryset to make it comparable to the list.
        # By default the callable is repr(), so you'll be probably see something like:
        # ['<Class: Object1>'], yes, with quotes because of this callable and
        # the test will fail though it seems that it shouldn't.
        # So since 1.5+, you should use the code below to get the expected result
        # Note: map(repr, queryset) applies the repr() to every element in the queryset
        self.assertQuerysetEqual(saved_user.answers.all().order_by('label'), map(repr, saved_answers))
