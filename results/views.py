from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import PCDPUser
from .serializers import PCDPUserSerializer

class ResultDetailView(APIView):

    def get(self, request, assessment_id):
        users_who_took_assessment = PCDPUser.objects.filter(answers__question__assessment_id=assessment_id).distinct()
        serializer = PCDPUserSerializer(users_who_took_assessment, many=True)
        return Response(serializer.data)
