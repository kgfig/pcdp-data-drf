from rest_framework import serializers

from assessment.serializers import AnswerSerializer
from results.models import PCDPUser

class PCDPUserSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True, read_only=True)
    
    class Meta:
        model = PCDPUser
        fields = ['id', 'username', 'original_id', 'project_id', 'firstname', 'lastname', 'email', 'answers']
        depth = 1
