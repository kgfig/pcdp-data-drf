from zipfile import ZipFile
import os

class Importer:

    DB_EXTENSION = 'sqlite3'
    TEMP_USER_LIB_DB = 'pcdp_temp_userlib.sqlite3'
    TEMP_USER_DB = 'pcdp_temp_user.sqlite3'

    def __init__(self, path):
        self.dir = os.path.dirname(path)
        self.zip = ZipFile(path)
        datafiles = self.zip.infolist()
        self.files = [ datafile for datafile in datafiles if datafile.filename.lower().endswith(Importer.DB_EXTENSION) ]

    def importAll(self):
        for file in self.files:
            self.importData(file, Importer.TEMP_USER_DB)
            
    def importData(self, file, temp_filename):
        self.zip.extract(file.filename, path=self.dir)
        
