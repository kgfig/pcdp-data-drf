from django.core.urlresolvers import reverse
from django.test import TestCase
import json
from rest_framework import status
from rest_framework.test import APITestCase

from assessment.models import Answer, Assessment, Question
from assessment.views import AssessmentDetailView

class AssessmentDetailViewTest(TestCase):

    def test_resolves_url_to_detail_view(self):
        assessment = Assessment(title='Test Assessment', original_id=0, type=3)
        assessment.save()
        
        response = self.client.get(reverse('assessment:detail', kwargs={'assessment_id': assessment.id}))
        self.assertEqual(response.status_code, 200)

class AssessmentAPITests(APITestCase):

    def test_assessment_detail_returns_expected_json(self):
        assessment = Assessment(title='Test Assessment', original_id=0, type=3)
        assessment.save()

        first_question = Question(text='Test Question #1', original_id=0, seq_num=1, points=0, assessment=assessment)
        first_question.save()
        
        first_answer = Answer(text='Choice A for #1', original_id=0, label='A', question=first_question, correct=True)
        second_answer = Answer(text='Choice B for #2', original_id=0, label='B', question=first_question, correct=False)
        first_answer.save()
        second_answer.save()
        
        expected_json = {
            'id': assessment.id,
            'title': assessment.title,
            'original_id': assessment.original_id,
            'questions': [
                {
                    'id': first_question.id,
                    'text': first_question.text,
                    'original_id': first_question.original_id,
                    'seq_num': first_question.seq_num,
                    'points': first_question.points,
                    'answers': [
                        {
                            'id': first_answer.id,
                            'text': first_answer.text,
                            'original_id': first_answer.original_id,
                            'label': first_answer.label,
                            'correct': first_answer.correct
                        },
                        {
                            'id': second_answer.id,
                            'text': second_answer.text,
                            'original_id': second_answer.original_id,
                            'label': second_answer.label,
                            'correct': second_answer.correct
                        },
                    ]
                },
            ]
        }
        
        url = reverse('assessment:detail', kwargs={'assessment_id': assessment.id,})
        response = self.client.get(url, format='json')
        # Now it works... the OrderedDict from DRF serializer is not the problem. The response simply does not match the correct json.
        # --- real cause: value for 'points' from the response was a decimal converted to string while the dict had an integer value
        self.assertEqual(response.data, expected_json) 
        # self.assertEqual(json.loads(response.content.decode('utf-8')), expected_json) # works too
