from django.core.urlresolvers import reverse
from django.test import TestCase
from rest_framework.test import APITestCase
import json

from assessment.models import Assessment, Question, Answer, Project
from results.models import PCDPUser

class UserDataDetailViewTest(TestCase):

    def test_resolves_url_to_detail_view(self):
        response = self.client.get(reverse('results:detail', kwargs={'assessment_id': 1}))
        self.assertEqual(response.status_code, 200)

    def test_detail_contains_assessment_results_for_related_users(self):
        project = Project(name='Sample Project')
        project.save()
        
        assessment = Assessment(title='Assessment Sample', original_id=0, type=3, project=project)
        assessment.save()

        first_question = Question(text='Question #1', original_id=0, seq_num=1, points=0, assessment=assessment)
        second_question = Question(text='Question #2', original_id=0, seq_num=2, points=0, assessment=assessment)
        third_question = Question(text='Question #3', original_id=0, seq_num=3, points=0, assessment=assessment)
        first_question.save()
        second_question.save()
        third_question.save()

        f_choice_a = Answer(text='Choice A', original_id=0, label='A', correct=True, question=first_question)
        f_choice_b = Answer(text='Choice B', original_id=0, label='B', correct=False, question=first_question)
        s_choice_a = Answer(text='Choice A', original_id=0, label='A', correct=False, question=second_question)
        s_choice_b = Answer(text='Choice B', original_id=0, label='B', correct=True, question=second_question)
        t_choice_a = Answer(text='Choice A', original_id=0, label='A', correct=True, question=third_question)
        t_choice_b = Answer(text='Choice B', original_id=0, label='B', correct=False, question=third_question)
        f_choice_a.save()
        f_choice_b.save()
        s_choice_a.save()
        s_choice_b.save()
        t_choice_a.save()
        t_choice_b.save()

        user = PCDPUser(firstname='Juanita', lastname='dela Cruz', original_id=0, username='juandc', email='juandc@pcdpgroup.com', project=project)
        other_user = PCDPUser(firstname='Pong', lastname='Pagong', original_id=0, username='pong', email='pong@pcdpgroup.com', project=project)
        user.save()
        other_user.save()

        user.answers.add(f_choice_a)
        user.answers.add(s_choice_b)
        user.answers.add(t_choice_a)
        other_user.answers.add(f_choice_b)
        other_user.answers.add(s_choice_b)
        other_user.answers.add(t_choice_b)
        user.save()
        other_user.save()

        expected_json = [
            {
                'id': user.id,
                'username': user.username,
                'original_id': user.original_id,
                'project_id': user.project.id,
                'firstname': user.firstname,
                'lastname': user.lastname,
                'email': user.email,
                'answers': [
                    {
                        'id': f_choice_a.id,
                        'text': f_choice_a.text,
                        'original_id': f_choice_a.original_id,
                        'label': f_choice_a.label,
                        'correct': f_choice_a.correct,
                    },
                    {
                        'id': s_choice_b.id,
                        'text': s_choice_b.text,
                        'original_id': s_choice_b.original_id,
                        'label': s_choice_b.label,
                        'correct': s_choice_b.correct,
                    },
                    {
                        'id': t_choice_a.id,
                        'text': t_choice_a.text,
                        'original_id': t_choice_a.original_id,
                        'label': t_choice_a.label,
                        'correct': t_choice_a.correct,                        
                    },
                ],
            },
            {
                'id': other_user.id,
                'username': other_user.username,
                'original_id': other_user.original_id,
                'project_id': other_user.project.id,
                'firstname': other_user.firstname,
                'lastname': other_user.lastname,
                'email': other_user.email,
                'answers': [
                    {
                        'id': f_choice_b.id,
                        'text': f_choice_b.text,
                        'original_id': f_choice_b.original_id,
                        'label': f_choice_b.label,
                        'correct': f_choice_b.correct,
                    },
                    {
                        'id': s_choice_b.id,
                        'text': s_choice_b.text,
                        'original_id': s_choice_b.original_id,
                        'label': s_choice_b.label,
                        'correct': s_choice_b.correct,
                    },
                    {
                        'id': t_choice_b.id,
                        'text': t_choice_b.text,
                        'original_id': t_choice_b.original_id,
                        'label': t_choice_b.label,
                        'correct': t_choice_b.correct,                        
                    },
                ],
            },
        ]

        response = self.client.get(reverse('results:detail', kwargs={'assessment_id': assessment.id}))
        self.maxDiff = None
        # self.assertEqual(json.loads(response.content.decode('utf-8')), expected_json)
        self.assertEqual(response.data, expected_json)
        
        
        
