from django.shortcuts import get_object_or_404, render
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Assessment
from .serializers import AssessmentSerializer

class AssessmentDetailView(APIView):

    def get(self, request, assessment_id):
        assessment = get_object_or_404(Assessment.objects.filter(pk=assessment_id))
        serializer = AssessmentSerializer(assessment)
        return Response(serializer.data)
