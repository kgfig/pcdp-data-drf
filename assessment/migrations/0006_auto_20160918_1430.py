# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-18 06:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('assessment', '0005_answer'),
    ]

    operations = [
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='choices', related_query_name='choice', to='assessment.Question'),
        ),
        migrations.AlterField(
            model_name='answer',
            name='label',
            field=models.CharField(blank=True, default=None, max_length=32, null=True),
        ),
    ]
