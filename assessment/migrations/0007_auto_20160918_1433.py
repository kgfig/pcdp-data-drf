# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-18 06:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('assessment', '0006_auto_20160918_1430'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='answers', related_query_name='answer', to='assessment.Question'),
        ),
    ]
