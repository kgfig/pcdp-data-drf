# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-18 06:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessment', '0004_auto_20160918_1409'),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(default='')),
                ('original_id', models.IntegerField(default=0)),
                ('correct', models.BooleanField(default=False)),
                ('label', models.CharField(default='', max_length=32)),
            ],
        ),
    ]
