from rest_framework import serializers

from .models import Answer, Assessment, Question

class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ['id', 'text', 'original_id', 'label', 'correct']

class QuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True, read_only=True)
    class Meta:
        model = Question
        fields = ['id', 'text', 'original_id', 'seq_num', 'points', 'answers']
        depth = 1

class AssessmentSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True, read_only=True)
        
    class Meta:
        model = Assessment
        fields = ['id', 'title', 'original_id', 'questions']
        depth = 1
