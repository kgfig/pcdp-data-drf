from django.conf.urls import url
from assessment.views import AssessmentDetailView

urlpatterns = [
    url(r'^(?P<assessment_id>\d+)/$', AssessmentDetailView.as_view(), name='detail')
]
