from django.test import TestCase
from assessment.models import Project

class ProjectModelTest(TestCase):

    def test_can_save_and_get_project(self):
        project = Project(name='')
        project.save()

        saved_project = Project.objects.first()
        self.assertEqual(saved_project, project)
