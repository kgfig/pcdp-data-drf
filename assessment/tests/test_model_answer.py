from django.test import TestCase
from assessment.models import Answer, Question

class AnswerModelTest(TestCase):

    def test_can_save_and_get_answer(self):
        answer = Answer(text='Choice A', original_id=0, correct=False)
        answer.label = 'A'
        answer.save()

        saved_answer = Answer.objects.first()
        self.assertEqual(saved_answer, answer)

    def test_can_be_added_to_queestion(self):
        question = Question(text='Test Question', original_id=0, points=0, seq_num=0)
        question.save()

        answer = Answer(text='Choice A', original_id=0, correct=False, question=question)
        answer.label = 'A'
        answer.save()

        saved_answer = Answer.objects.first()
        self.assertEqual(saved_answer, answer)

    def test_fetch_answers_by_question_returns_only_related_answers(self):
        question = Question(text='Test Question', original_id=0, points=0, seq_num=0)
        question.save()
        
        answer = Answer(text='Choice A', original_id=0, correct=True, label='A', question=question)
        answer.save()

        other_answer = Answer(text='Choice B', original_id=0, correct=False, label='B', question=question)
        other_answer.save()

        saved_choices = Answer.objects.filter(question=question)
        self.assertEqual(list(saved_choices), [answer, other_answer])

