from django.test import TestCase
from assessment.models import Assessment, Project

class AssessmentModelTest(TestCase):

    def test_can_save_and_get_assessment(self):
        assessment = Assessment(title='Test Assessment', original_id=0, type=3)
        assessment.save()

        saved_assessment = Assessment.objects.first()
        self.assertEqual(saved_assessment, assessment, msg='Saved assessment object w/pk=%d does not match argument w/pk=%d' % (assessment.pk, saved_assessment.pk,))

    def test_assessment_can_be_associated_with_a_project(self):
        project = Project(name='Very Important Project')
        project.save()
        
        assessment = Assessment(title='Test Assessment', original_id=0, type=3, project=project)
        assessment.save()

        another_assessment = Assessment(title='Another Assessment', original_id=0, type=3)
        another_assessment.save()

        self.assertEqual(assessment.project, project)
        self.assertIsNone(another_assessment.project)
