from django.test import TestCase
from assessment.models import Assessment, Question

class QuestionTest(TestCase):

    def test_can_save_and_get_question(self):
        question = Question(text='Test Question', original_id=0, points=0, seq_num=0)
        question.save()

        saved_question = Question.objects.first()
        self.assertEqual(saved_question, question)
        
    def test_can_add_question_to_assessment(self):
        assessment = Assessment(title='This is a new assessment', original_id=0)
        assessment.save()
        
        question = Question(text='Test Question', original_id=0, points=0, seq_num=0)
        question.assessment = assessment
        question.save()

        saved_question = Question.objects.first()
        self.assertEqual(saved_question.assessment, assessment)


    def test_can_get_questions_for_an_assessment(self):
        assessment = Assessment(title='This is a new assessment', original_id=0)
        assessment.save()

        first_question = Question(text='Test Question #1', original_id=0, points=0, seq_num=1, assessment=assessment)
        first_question.save()

        next_question = Question(text='Test Question #2', original_id=0, points=0, seq_num=2, assessment=assessment)
        next_question.save()

        num_questions_added = 2
        assessment_questions = Question.objects.filter(assessment=assessment)
        self.assertEqual(list(assessment_questions), [first_question, next_question])

    

        

        
