from django.db import models

class Project(models.Model):
    name = models.CharField(max_length=128, default='')

    def __str__(self):
        return '%d:%s' % (self.id, self.name)

class Assessment(models.Model):
    title = models.CharField(max_length=64, default='')
    original_id = models.IntegerField(default=0)
    type = models.IntegerField(default=0, blank=True, null=True)
    project = models.ForeignKey(
        Project,
        default=None,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='assessment_list',
        related_query_name='assessment'
    )

    def __str__(self):
        return '%d: %s' % (self.original_id, self.title)
    
class Question(models.Model):
    text = models.TextField(default='')
    original_id = models.IntegerField(default=0)
    points = models.IntegerField(default=0)
    seq_num = models.IntegerField(default=0)
    assessment = models.ForeignKey(
        Assessment,
        default=None,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name='questions',
        related_query_name='question'
    )


    def __str__(self):
        return '%d: %s' % (self.original_id, self.text,)


class Answer(models.Model):
    text = models.TextField(default='')
    original_id = models.IntegerField(default=0)
    correct = models.BooleanField(default=False)
    label = models.CharField(max_length=32, default=None, blank=True, null=True)
    question = models.ForeignKey(
        Question,
        default=None,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name='answers',
        related_query_name='answer'
    )

    def __str__(self):
        return '%d:%s %s:%s' % (self.original_id, self.correct, self.label, self.text,)
